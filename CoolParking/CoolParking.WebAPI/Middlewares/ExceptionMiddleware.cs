﻿using Microsoft.AspNetCore.Http;
using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType = "application/json";

                switch (error)
                {
                    case ArgumentException e:
                        // not found error
                        if (e.Message == "Bad ID!")
                            response.StatusCode = (int)HttpStatusCode.BadRequest;
                        if (e.Message == "Bad ID format!")
                            response.StatusCode = (int)HttpStatusCode.BadRequest;
                        if (e.Message == "Vehicle not found!")
                            response.StatusCode = (int)HttpStatusCode.NotFound;
                        if (e.Message == "Bad arguments!")
                            response.StatusCode = (int)HttpStatusCode.BadRequest;
                        break;
                    case InvalidOperationException e:
                        if (e.Message == "Parking is full!")
                            response.StatusCode = (int)HttpStatusCode.Forbidden;
                        if (e.Message == "Vehicle balance is negative!")
                            response.StatusCode = (int)HttpStatusCode.Forbidden;
                        if (e.Message == "File not found!")
                            response.StatusCode = (int)HttpStatusCode.NotFound;
                        break;
                    default:
                        // unhandled error
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        break;
                }

                var result = JsonSerializer.Serialize(new { message = error?.Message });
                await response.WriteAsync(result);
            }
        }
    }
}
