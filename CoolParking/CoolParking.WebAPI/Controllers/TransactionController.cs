﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using AutoMapper;
using System.Text.RegularExpressions;
using CoolParking.Shared.DTOs;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TransactionsController : ControllerBase
    {
        private IParkingService _parkingService;
        
        public TransactionsController(IParkingService service)
        {
            _parkingService = service;
        }

        //GET api/transactions/last
        [HttpGet("last")]
        public ActionResult<IEnumerable<GetTransactionsDTO>> GetLastParkingTransactions()
        {
            var lastTransactions = _parkingService.GetLastParkingTransactions();
            var config = new MapperConfiguration(cfg =>
            cfg.CreateMap<TransactionInfo, GetTransactionsDTO>()
            .ForMember(dest => dest.DateTime, act => act.MapFrom(src => src.DateTime))
            );
            var mapper = new Mapper(config);
            List<GetTransactionsDTO> lastTransactionsDTO = new List<GetTransactionsDTO>();
            return Ok(mapper.Map<IEnumerable<GetTransactionsDTO>>(lastTransactions));
        }
        //GET api/transactions/all
        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            return Ok(_parkingService.ReadFromLog());
        }
        //PUT api/transactions/topUpVehicle
        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle([FromBody] TopUpVehicleDTO content)
        {   
            if(!Regex.IsMatch(content.Id, @"^([A-Z]{2})-([0-9]{4})-([A-Z]{2})$"))
            {
                return BadRequest("Bad ID format!");
            }
            var vehicle = _parkingService.GetVehicle(content.Id);
            if(vehicle == null)
            {
                return NotFound("Vehicle not found!");
            }
            if (content.Balance == null)
            {
                return BadRequest("Bad body request!");
            }
            _parkingService.TopUpVehicle(content.Id, (decimal)content.Balance);
            return Ok(vehicle);
        }
    }
}
