﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ParkingController : ControllerBase
    {
        private IParkingService _parkingService;
        public ParkingController(IParkingService service)
        {
            _parkingService = service;
        }

        //GET api/parking/balance
        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(_parkingService.GetBalance());
        }
        //GET api/parking/capacity
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(_parkingService.GetCapacity());
        }
        //GET api/parking/freePlaces
        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(_parkingService.GetFreePlaces());
        }
        [HttpGet("income")]
        public ActionResult<decimal> GetCurrentIncome()
        {
            return Ok(_parkingService.GetCurrentIncome());
        }
    }
}
