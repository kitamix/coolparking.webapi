﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using AutoMapper;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class VehiclesController : ControllerBase
    {
        private IParkingService _parkingService;

        public VehiclesController(IParkingService service)
        {
            _parkingService = service;
        }

        //GET api/vehicles
        [HttpGet]
        public IReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parkingService.GetVehicles(); 
        }
        //GET api/vehicles/id
        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicleById(string id)
        {
            if (!Regex.IsMatch(id, @"^([A-Z]{2})-([0-9]{4})-([A-Z]{2})$"))
            {
                return BadRequest("Bad ID format!");
            }
            var vehicles = _parkingService.GetVehicle(id);
            if (vehicles == null)
            {
                return NotFound("Vehicle not found!");
            }
            return Ok(_parkingService.GetVehicle(id));
        }
        //POST api/vehicles
        [HttpPost]
        public ActionResult<Vehicle> AddVehicle([FromBody] Vehicle vehicle)
        {
            if (!(Regex.IsMatch(vehicle.Id, @"^([A-Z]{2})-([0-9]{4})-([A-Z]{2})$"))
                || (int)vehicle.VehicleType > 3
                || (int)vehicle.VehicleType < 0
                || vehicle.Balance < 0)
            {
                return BadRequest("Wrong request body!");
            }
            _parkingService.AddVehicle(vehicle);
            HttpContext.Response.StatusCode = 201;
            return vehicle;
        }
        //DELETE api/vehicles/id
        [HttpDelete("{id}")]

        public ActionResult RemoveVehicle(string id)
        {
            if (!Regex.IsMatch(id, @"^([A-Z]{2})-([0-9]{4})-([A-Z]{2})$"))
            {
                return BadRequest("Bad ID format!");
            }
            var vehicle = _parkingService.GetVehicle(id);
            if (vehicle == null)
            {
                return NotFound("Vehicle not found!");
            }
            _parkingService.RemoveVehicle(id);
            return NoContent();
        }
    }
}
