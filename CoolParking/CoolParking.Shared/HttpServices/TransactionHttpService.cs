﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.Shared.Models;
using Newtonsoft.Json;
using System.Net.Http;

namespace CoolParking.Shared.HttpServices
{
    public class TransactionHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();
        
        public TransactionHttpService()
        {
            _httpClient.BaseAddress = new Uri("http://localhost:5000/api/transactions/");
        }

        public async Task<List<LastTransactions>> GetLastParkingTransactions()
        {
            var content = await _httpClient.GetStringAsync("last");
            return JsonConvert.DeserializeObject<List<LastTransactions>>(content);
        }

        public async Task<string> GetAllTransactions()
        {
            var content = await _httpClient.GetStringAsync("all");
            return content;
        }

        public async Task<HttpResponseMessage> TopUpVehicle(string vehicleId, decimal Sum)
        {
            var content = JsonConvert.SerializeObject(new VehicleTopUp(vehicleId, Sum));
            var httpContent = new StringContent(content, Encoding.UTF8, "application/json");
            return await _httpClient.PutAsync("topUpVehicle", httpContent);

        }
    }
}
