﻿namespace CoolParking.Shared.Models
{
    public enum VehicleType
    {
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}