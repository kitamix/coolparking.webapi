﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Shared.Models
{
    public class LastTransactions
    {
        public decimal Sum { get; set; }
        public DateTime DateTime { get; set; }
        public string VehicleId { get; set; }
    }
}
