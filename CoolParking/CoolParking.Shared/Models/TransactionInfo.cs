﻿using Newtonsoft.Json;
using System;

namespace CoolParking.Shared.Models
{
    public class TransactionInfo
    {
        [JsonProperty("sum")]
        public decimal Sum { get; }
        [JsonProperty("transactionDate")]
        public DateTime DateTime { get; }
        [JsonProperty("vehicleId")]
        public string VehicleId { get; }
    }
}