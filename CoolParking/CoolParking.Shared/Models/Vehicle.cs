﻿using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.Shared.Models
{
    public class Vehicle
    {
        [JsonProperty("id")]
        public string Id { get; }
        [JsonProperty("vehicletype")]
        public VehicleType VehicleType { get; }
        [JsonProperty("balance")]
        public decimal Balance { get; internal set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            VehicleType = vehicleType;
            Balance = balance;
            Id = id;
        }

    }
}