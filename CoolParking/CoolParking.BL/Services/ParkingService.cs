﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using System.Linq;
using System.Timers;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking _parking;
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private List<TransactionInfo> _transactions;
        private readonly ILogService _logService;

        public ParkingService()
        {
            _parking = Parking.GetParking();
            _parking.CurrentIncome = 0M;
            _withdrawTimer = new TimerService();
            _withdrawTimer.Interval = Settings.WithdrawInterwal;
            _withdrawTimer.Elapsed += AddNewParkingTransaction;
            _withdrawTimer.Start();
            _logTimer = new TimerService();
            _logTimer.Interval = Settings.LogInterval;
            _logTimer.Elapsed += LogNewParkingTransactions;
            _logTimer.Start();
            _logService = new LogService(Settings.LogFilePath);
            _transactions = new List<TransactionInfo>();
        }
            public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.GetParking();
            _parking.CurrentIncome = 0M;
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Interval = Settings.WithdrawInterwal;
            _withdrawTimer.Elapsed += AddNewParkingTransaction;
            _withdrawTimer.Start();
            _logTimer = logTimer;
            _logTimer.Interval = Settings.LogInterval;
            _logTimer.Elapsed += LogNewParkingTransactions;
            _logTimer.Start();
            _logService = logService;
            _transactions = new List<TransactionInfo>();
        }
        private void AddNewParkingTransaction(object source, ElapsedEventArgs e)
        {
            foreach(var vehicle in _parking.Vehicles)
            {
                decimal price = Settings.Withdraw[vehicle.VehicleType];

                if (vehicle.Balance < price && vehicle.Balance > 0)
                {
                    price = vehicle.Balance + ((price - vehicle.Balance)*Settings.FineCoefficient);
                }
                else if (vehicle.Balance < price) 
                {
                    price *= Settings.FineCoefficient;
                }

                vehicle.Balance -= price;
                _parking.Balance += price;
                var transaction = new TransactionInfo(vehicle.Id, price);
                _transactions.Add(transaction);
                _parking.CurrentIncome += price;
            }
        }

        private void LogNewParkingTransactions(object source, ElapsedEventArgs e)
        {
            var logInfo = string.Empty;

            foreach (var transaction in _transactions)
            {
                //logInfo += transaction.TransactionMessage;
                logInfo += $"[{transaction.DateTime}]: New transaction for vehicle {transaction.VehicleId} was created. Transaction Sum: {transaction.Sum}\n";
            }

            _transactions.Clear();
            _parking.CurrentIncome = 0M;
            _logService.Write(logInfo);
        }

        public void AddVehicle(Vehicle vehicle)
        {

            if (_parking.Vehicles.Count() == Settings.ParkingCapacity)
            {
                throw new InvalidOperationException("Parking is full!");
            }
            if (_parking.Vehicles.Count(x => x.Id == vehicle.Id) > 0)
            {
                throw new ArgumentException("Bad ID!");
            }
            _parking.Vehicles.Add(vehicle);
        }
        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _parking.CleanParking();
            _parking.Balance = Settings.StartBalance;
        }

        public decimal GetBalance() => _parking.Balance;

        public decimal GetCurrentIncome() => _parking.CurrentIncome;

        public int GetCapacity() => Settings.ParkingCapacity;

        public int GetFreePlaces() => Settings.ParkingCapacity - _parking.Vehicles.Count;

        public TransactionInfo[] GetLastParkingTransactions() => _transactions.ToArray();

        public ReadOnlyCollection<Vehicle> GetVehicles() => new ReadOnlyCollection<Vehicle>(_parking.Vehicles);

        public Vehicle GetVehicle(string id) => _parking.Vehicles.Find(x => x.Id == id);

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault(vehicle => vehicle.Id == vehicleId);
            if (!Regex.IsMatch(vehicleId, @"^([A-Z]{2})-([0-9]{4})-([A-Z]{2})$"))
            {
                throw new ArgumentException("Bad ID format!");
            }
            if (vehicle == null)
            {
                throw new ArgumentException("Vehicle not found!");
            }
            if (vehicle.Balance < 0)
            {
                throw new InvalidOperationException("Vehicle balance is negative!");
            }
            _parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault(vehicle => vehicle.Id == vehicleId);
            if (vehicle == null || sum < 0)
            {
                throw new ArgumentException("Bad arguments!");
            }
            vehicle.Balance += sum;
        }
    }
}