﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; }
       // public string TransactionMessage { get; }
        public DateTime DateTime { get; }
        public string VehicleId { get; }
        public TransactionInfo(string vehicleId, decimal sum)
        {
            VehicleId = vehicleId;
            Sum = sum;
            DateTime = DateTime.Now;
            //TransactionMessage = $"[{DateTime}]: New transaction for vehicle {vehicleId} was created. Transaction Sum: {sum}\n";
        }
    }
}