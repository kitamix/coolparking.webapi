﻿using System;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [JsonProperty("id")]
        public string Id { get; }
        [JsonProperty("vehicletype")]
        public VehicleType VehicleType { get; }
        [JsonProperty("balance")]
        public decimal Balance { get;internal set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (id == null)
            {
                throw new ArgumentException("Bad ID!");
            }
            var regex = new Regex($"[A-z][A-z]-[0-9][0-9][0-9][0-9]-[A-z][A-z]");
            if (regex.IsMatch(id) && balance > 0)
            {
                Id = id.ToUpper();
            }
            else
            {
                throw new ArgumentException("Bad ID format!");
            }
            VehicleType = vehicleType;
            Balance = balance;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            string id = string.Empty;
            string idpart1 = string.Empty;
            string idpart2 = string.Empty;
            char part1, part2;
            Random random = new Random();
            for (int i = 0; i < 2; i++)
            {
                part1 = (char)random.Next('A', 'Z');
                idpart1 += part1;
                part2 = (char)random.Next('A', 'Z');
                idpart2 += part2;
            }
            int num = random.Next(0000,9999);
            id += $"{idpart1}-{num}-{idpart2}";
            return id;
        }
    }
}