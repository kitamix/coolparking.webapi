﻿using CoolParking.BL.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
namespace CoolParking.BL.Models
{
    class Parking
    {
        private static Parking parking;
        public List<Vehicle> Vehicles { get; set; }
        public decimal Balance { get; set; }
        public decimal CurrentIncome { get; set; }
        private Parking()
        {
            Vehicles = new List<Vehicle>();
            Balance = Settings.StartBalance;
        }
        public static Parking GetParking()
        {
            if (parking == null)
                parking = new Parking();
            return parking;
        }
        public void CleanParking()
        {
            Vehicles = new List<Vehicle>();
        }
    }
}