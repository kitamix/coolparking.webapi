﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CoolParking.ConsoleApp.Models;


namespace CoolParking.ConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Menu.Start();
        }
    }
}
