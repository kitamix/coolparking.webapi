﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.Shared.HttpServices;
using CoolParking.Shared.Models;
namespace CoolParking.ConsoleApp.Models
{
    public static class Menu
    {
        private static ParkingHttpService _parkingService = new ParkingHttpService();
        private static VehiclesHttpService _vehiclesService = new VehiclesHttpService();
        private static TransactionHttpService _transactionService = new TransactionHttpService();
        public static void Start()
        {
           Console.Clear();
            Console.WriteLine("---###MENU###---");
            Console.WriteLine("1. Show parking balance");
            Console.WriteLine("2. Show income for the current period");
            Console.WriteLine("3. Show parking capacity");
            Console.WriteLine("4. Show transactions for the current period");
            Console.WriteLine("5. Show transactions history");
            Console.WriteLine("6. Show vehicles, currently placed on parking");
            Console.WriteLine("7. Place vehicle on parking");
            Console.WriteLine("8. Remove vehicle on parking");
            Console.WriteLine("9. Top up vehicle balance");
            Console.WriteLine("10. Exit");
            Console.Write("\nWrite choosen menu option: ");
            var menuKey = Int32.Parse(Console.ReadLine());
            Console.Clear();
            switch (menuKey)
            {
                case 1:
                    ShowParkingBalance();
                    break;
                case 2:
                    ShowCurrentIncome();
                    break;
                case 3:
                    ShowParkingCapacity();
                    break;
                case 4:
                    ShowCurrentTransactions();
                    break;
                case 5:
                    ShowTransactionsHistory();
                    break;
                case 6:
                    ShowVehicles();
                    break;
                case 7:
                    PlaceVehicle();
                    break;
                case 8:
                    RemoveVehicle();
                    break;
                case 9:
                    TopUpVehicle();
                    break;
                case 10:
                    Environment.Exit(0);
                    break;
                default:
                    throw new Exception("Choosen munu option must be in range 1 to 10");


            }
            Back();
        }
        public static void ShowParkingBalance()
        {
           
            Console.WriteLine($"Current parking balance: {_parkingService.GetBalance().GetAwaiter().GetResult()}");
            
        }
        public static void ShowCurrentIncome()
        {
           
            Console.WriteLine($"Current parking income: {_parkingService.GetCurrentIncome().GetAwaiter().GetResult()}");
            
        }
        public static void ShowParkingCapacity()
        {
           
            var capacity = _parkingService.GetCapacity().GetAwaiter().GetResult();
            var freePlaces = _parkingService.GetFreePlaces().GetAwaiter().GetResult();
            Console.WriteLine($"Free places on parking: {freePlaces}\n" +
                $"Occupied places on parking: {capacity - freePlaces}\n" +
                $"Total places on parking: {capacity}");
            
        }
        public static void ShowCurrentTransactions()
        {
           
            var transactions = _transactionService.GetLastParkingTransactions().GetAwaiter().GetResult();
            foreach (var transaction in transactions)
            {
                Console.WriteLine("[{0}]: New transaction for vehicle {1} was created. Transaction Sum: {2}\n",
                   transaction.DateTime.ToString(),
                   transaction.VehicleId.ToString(),
                   transaction.Sum.ToString());
            }
            
        }
        public static void ShowTransactionsHistory()
        {
           
            string transactions = _transactionService.GetAllTransactions().GetAwaiter().GetResult();
            var transactionList = transactions.Replace("\\r", "").Trim('"').Split("\\n", StringSplitOptions.RemoveEmptyEntries).ToList();
            transactionList.ForEach(transaction => Console.WriteLine(transaction));
            
        }
        public static void PlaceVehicle()
        {
          
            Console.Write("Write new vehicle iD it format \"XX-YYYY-XX\", where X - letter, Y - number: ");
            var vehicleId = Console.ReadLine();
            Console.Write("\nChoose Vehicle Type (1 - PassengerCar, 2 - Truck, 3 - Bus, 4 - Motorcycle): ");
            if(!Int32.TryParse(Console.ReadLine(), out int vehicleType) || (vehicleType > 4) || (vehicleType < 1))
            {
                throw new ArgumentException("Bad input!");
            } 
            Console.Write("\nWrite start vehicle balance: ");
            if(!decimal.TryParse(Console.ReadLine(), out decimal Balance))
            {
                throw new ArgumentException("Bad input!");
            }
            var response = _vehiclesService.AddVehicle(new Vehicle(vehicleId, (VehicleType)vehicleType - 1, Balance)).GetAwaiter().GetResult();
            if ((int)response.StatusCode == 400)
            {
                throw new Exception("Request body is invalid!");
            }
            else if ((int)response.StatusCode == 403)
            {
                throw new Exception("Vehicle with same ID currently placed on parking!");
            }
            else
            {
                Console.WriteLine("\nVehicle was placed successfully!");
            }
            
        }
        public static void ShowVehicles()
        {
           
            List();
            
        }
        public static void RemoveVehicle()
        {
            List();
            Console.Write("\nWrite vehicle ID to remove: ");
            var vehicleId = Console.ReadLine();
            var response = _vehiclesService.RemoveVehicle(vehicleId).GetAwaiter().GetResult();
            if ((int)response.StatusCode == 400)
            {
                throw new Exception("Vehicle ID is invalid!");
            }
            if ((int)response.StatusCode == 404)
            {
                throw new Exception("Vehicle not found!");
            }
            Console.WriteLine("Vehicle removed successfully!");
        }
        public static void TopUpVehicle()
        {
           
            List();
            Console.Write("\nWrite Vehicle ID to TopUp: ");
            var vehicleId = Console.ReadLine();
            Console.Write("Write top up amount: ");
            decimal.TryParse(Console.ReadLine(), out decimal sum);
            var response = _transactionService.TopUpVehicle(vehicleId, sum).GetAwaiter().GetResult();
            if ((int)response.StatusCode == 400)
            {
                throw new Exception("Request body is invalid!");
            }
            if ((int)response.StatusCode == 404)
            {
                throw new Exception("Vehicle not found!");
            }
            Console.WriteLine("Topped up successfully!");
            
        }
        public static void List()
        {
            var vehicles = _vehiclesService.GetVehicles().GetAwaiter().GetResult();
            var counter = 1;
            Console.WriteLine($"Spot № \t|\tID\t\t|\tBalance\t|\tType");
            foreach (var vehicle in vehicles)
            {
                    Console.WriteLine($"{counter} \t|\t{vehicle.Id}\t|\t{vehicle.Balance}\t|\t{vehicle.VehicleType}");
                    counter += 1;
            }
        }
       
        public static void Back()
        {
            Console.WriteLine("\nPress any key to go back...");
            Console.ReadKey();
            Start();
        }
        
    }   
}
